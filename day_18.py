import argparse
from typing import TextIO
from math import floor, ceil
from copy import deepcopy


class Node:
    def __init__(self):
        self.value = None
        self.left_child = None
        self.right_child = None

    def is_leaf(self):
        return self.left_child is None and self.right_child is None


def parse_node(text, index):
    node = Node()
    character = text[index]
    if character == '[':
        index += 1
        node.left_child, index = parse_node(text, index)
        node.right_child, index = parse_node(text, index)
    elif character.isdigit():
        buffer = ''
        while character.isdigit():
            buffer += character
            index += 1
            character = text[index]
        node.value = int(buffer)
        while character in [',', ']'] and index < len(text) - 1:
            index += 1
            character = text[index]
    else:
        raise RuntimeError('Unknown character in input')

    return node, index


def add(root_a, root_b):
    node = Node()
    node.left_child = root_a
    node.right_child = root_b
    return node


def try_split(root):
    if root is None:
        return False
    if not root.is_leaf():
        applied = try_split(root.left_child)
        if applied:
            return True
        applied = try_split(root.right_child)
        if applied:
            return True
    else:
        if root.value > 9:
            left = floor(root.value / 2)
            right = ceil(root.value / 2)
            root.value = None
            root.left_child = Node()
            root.left_child.value = left
            root.right_child = Node()
            root.right_child.value = right
            return True
    return False


def try_explode(root, current, level):
    if current is None:
        return False
    if not current.is_leaf():
        if current.left_child.is_leaf() and current.right_child.is_leaf():
            if level > 3:
                tree_list = []
                tree_to_list(root, tree_list)
                for index, node in enumerate(tree_list):
                    if node is current.left_child:
                        prev_index = index - 1
                        next_index = index + 2
                        if prev_index >= 0:
                            tree_list[prev_index].value += current.left_child.value
                        if next_index < len(tree_list):
                            tree_list[next_index].value += current.right_child.value
                        current.value = 0
                        current.left_child = None
                        current.right_child = None
                        return True
        else:
            applied = try_explode(root, current.left_child, level + 1)
            if applied:
                return True
            applied = try_explode(root, current.right_child, level + 1)
            if applied:
                return True


def print_tree(root):
    if root is None:
        return
    if not root.is_leaf():
        print_tree(root.left_child)
        print_tree(root.right_child)
    else:
        print(root.value)


def tree_to_list(root, l):
    if root is None:
        return []
    if not root.is_leaf():
        l.extend(tree_to_list(root.left_child, []))
        l.extend(tree_to_list(root.right_child, []))
        return l
    else:
        l.append(root)
        return l


def get_magnitude(root):
    if root is None:
        return 0
    if not root.is_leaf():
        return 3 * get_magnitude(root.left_child) + 2 * get_magnitude(root.right_child)
    return root.value


def part_1(input_file: TextIO):
    root, _ = parse_node(input_file.readline(), 0)
    lines = input_file.read().split('\n')
    for line in lines:
        next_root, _ = parse_node(line, 0)
        root = add(root, next_root)

        exploded = True
        split = True
        while exploded or split:
            exploded = try_explode(root, root, 0)
            if exploded:
                continue
            split = try_split(root)
    print_tree(root)
    return get_magnitude(root)


def part_2(input_file: TextIO):
    roots = []
    for line in input_file.read().split('\n'):
        root, _ = parse_node(line, 0)
        roots.append(root)
    magnitudes = []
    for root_a in roots:
        for root_b in roots:
            if root_a is root_b:
                continue
            root = add(deepcopy(root_a), deepcopy(root_b))
            exploded = True
            split = True
            while exploded or split:
                exploded = try_explode(root, root, 0)
                if exploded:
                    continue
                split = try_split(root)
            magnitudes.append(get_magnitude(root))
    return max(magnitudes)


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 18')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
